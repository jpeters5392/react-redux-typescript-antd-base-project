import { ICitybikModuleName } from './_citybikInterfaces';

export const ModuleName: ICitybikModuleName = 'citybik';

export const SET_CITYBIK_NETWORKS_LIST = 'module/citybik/SET_CITYBIK_NETWORKS_LIST';
export const SET_CURRENT_CITYBIK_NETWORK = 'module/citybik/SET_CURRENT_CITYBIK_NETWORK';
export const SET_CURRENT_CITYBIK_NETWORK_LOADING_STATUS = 'module/citybik/SET_CURRENT_CITYBIK_NETWORK_LOADING_STATUS';
