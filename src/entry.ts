// load main application source file
import './app';

// load main style sheet
import './scss/index.scss';

// load helper style sheets for packages importing local files
import './assets/fonts/iconfont/iconfont.css';
